# README #

Control and distribution of electricity

### What is this repository for? ###

* It's a project of control and distribution of electric energy
* This project presents the service/RESTful layer

### How do I get set up? ###

* Use the file "resources/application.yml" to change mongo database config
* To add baseline records, it is necessary to include in the script "resources/db/inserts.js"
* For compile you will need maven 3, Java 8(jdk) and execute in bash(on root folder): 
```
$ mvn clean install
```
* For running you will need execute in bash (on root folder): 
```
$ java -jar target/ws-0.0.1-SNAPSHOT.jar
```

### API Documentation ###
* http://IP:PORT/api/swagger-ui.html
* This API uses JWT Authentication

	Authentication is required for any operation:
	
		Therefore use the endpoint POST "/api/login" to get the token, in the documentation inside the swagger has an example call:
	- "http://localhost:8080/api/swagger-ui.html#!/3532Login47Auth32Token/tokenAuthPOST"
* You will need to pass the token obtained in authentication in all requests