//mongeez formatted javascript
//changeset antonio:ChangeSet-1
function getPoint(arr){
    return {
       "type" : "Point",
       "coordinates" : [arr[0], arr[1]]
    }
}

db.authority.insert({"name": "ROLE_USER"})
db.authority.insert({"name": "ROLE_ADMIN"})

db.users.insert({
    "location": getPoint([-48.285513, -18.896928]),
    "powerConsumptionPerMinute": 5.00,
    "createdAt": new Date(),
    "username": "antonio",
    "password": "xpto",
    "firstname": "antonio",
    "lastname": "filho",
    "authorities": [db.authority.findOne({"name": "ROLE_USER"}), db.authority.findOne({"name": "ROLE_ADMIN"})]
})


var locationOne = [-48.293152, -18.896867];
var locationTwo = [-48.286629, -18.889558];
var locationThree = [-48.277702, -18.895081];
var locationFour = [-48.285084, -18.909372];
function polygon(){
    return {
        "type" : "Polygon",
        "coordinates" : [
            [
                locationOne,
                locationTwo,
                locationThree,
                locationFour
            ]
        ]
    }
}


var group = {name: "roosevelt", description: "group of roosevelt neighborhood", locations: polygon()};
db.groupStorageEquipaments.insert(group);


function example(){
    return {
        name: "equipament",
        location: getPoint(locationOne),
        groupStorageEquipment: db.groupStorageEquipaments.findOne(group)
    };
}

var example = example();
example['name'] = 'equipament one';
var one = example;
db.storageEquipaments.insert(one);
example['location'] = getPoint(locationTwo);
example['name'] = 'equipament two';
var two = example;
db.storageEquipaments.insert(two)
example['location'] = getPoint(locationThree);
example['name'] = 'equipament three';
var three = example;
db.storageEquipaments.insert(three);
example['location'] = getPoint(locationFour);
example['name'] = 'equipament four';
var four = example;
db.storageEquipaments.insert(four);

group = db.groupStorageEquipaments.findOne(group);
group['storageEquipments'] = [one,two,three,four];
db.groupStorageEquipaments.save(group);


db.storageEquipaments.createIndex({location:"2dsphere"});
