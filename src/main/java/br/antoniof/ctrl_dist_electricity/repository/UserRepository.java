package br.antoniof.ctrl_dist_electricity.repository;

import br.antoniof.ctrl_dist_electricity.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
    User findByUsername( String username );
}

