package br.antoniof.ctrl_dist_electricity.repository;

import br.antoniof.ctrl_dist_electricity.model.User;
import br.antoniof.ctrl_dist_electricity.model.UserPowerConsumption;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by antonio on 24/12/17.
 */
public interface UserPowerConsumptionRepository extends MongoRepository<UserPowerConsumption, String> {
    UserPowerConsumption findByUserAndEndIsNull(User user);
    List<UserPowerConsumption> findByUser(User user);
}

