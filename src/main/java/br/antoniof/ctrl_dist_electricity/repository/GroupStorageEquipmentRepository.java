package br.antoniof.ctrl_dist_electricity.repository;

import br.antoniof.ctrl_dist_electricity.model.GroupStorageEquipment;
import br.antoniof.ctrl_dist_electricity.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by antonio on 24/12/17.
 */
@Repository
public interface GroupStorageEquipmentRepository extends MongoRepository<GroupStorageEquipment, String> {
}

