package br.antoniof.ctrl_dist_electricity.repository;

import br.antoniof.ctrl_dist_electricity.model.StorageEquipment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by antonio on 24/12/17.
 */
@Repository
public interface StorageEquipmentRepository extends MongoRepository<StorageEquipment, String> {

    @Query(value = "{\"location\":\n" +
            "       { $near :\n" +
            "          {\n" +
            "            $geometry : {\n" +
            "               type : \"Point\" ,\n" +
            "               coordinates : [ ?0, ?1] }\n" +
            "          }\n" +
            "       }}")
    List<StorageEquipment> nearByLocation(double longitude, double latitude);
}

