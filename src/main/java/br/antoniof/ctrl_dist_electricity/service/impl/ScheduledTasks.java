package br.antoniof.ctrl_dist_electricity.service.impl;

import br.antoniof.ctrl_dist_electricity.model.GroupStorageEquipment;
import br.antoniof.ctrl_dist_electricity.model.StorageEquipment;
import br.antoniof.ctrl_dist_electricity.model.User;
import br.antoniof.ctrl_dist_electricity.model.UserPowerConsumption;
import br.antoniof.ctrl_dist_electricity.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by antonio on 24/12/17.
 */
@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserPowerConsumptionRepository userPowerConsumptionRepository;
    @Autowired
    private StorageEquipmentRepository storageEquipmentRepository;

    @Scheduled(cron = "0 * * * * ?")
    public void powerConsumptionPerMinute() {
        increasePowerOfEquipaments();
        List<User> users = userRepository.findAll();
        if(users != null){
            users.forEach(user -> businessRuleEachUser(user));
        }

        logger.info("Cron Task :: Execution Time - {}", dateFormat.format(new Date()));
    }

    private void increasePowerOfEquipaments(){
        List<StorageEquipment> storageEquipments = storageEquipmentRepository.findAll();
        if(storageEquipments != null){
            logger.info(String.format("find %d storageEquipments to increase", storageEquipments.size()));
            storageEquipments.stream().forEach(storageEquipment -> increaseAndPersist(storageEquipment));
        }else{
            logger.info("doesn't has equipament to increase power");
        }
    }

    private void increaseAndPersist(StorageEquipment storageEquipment){
        logger.info(String.format("increased energy for equipment '%s'",storageEquipment.getName() == null ? "undefined" : storageEquipment.getName()));
        storageEquipment.increase();
        storageEquipment = storageEquipmentRepository.save(storageEquipment);
    }

    private List<StorageEquipment> getStorageEquipamentByUserLocation(User user){
        List<StorageEquipment> storageEquipments = storageEquipmentRepository
                .nearByLocation(user.getLocation().getX(), user.getLocation().getY());
        return  storageEquipments== null ? new ArrayList<StorageEquipment>() : storageEquipments;
    }

    private void businessRuleEachUser(User user){
        Optional<StorageEquipment> equipmentForUse = getStorageEquipamentByUserLocation(user)
                .stream()
                .filter(storageEquipment -> storageEquipment.getCurrent() > user.getPowerConsumptionPerMinute())
                .findFirst();

        if(!equipmentForUse.isPresent()){
            logger.warn(String.format("no available equipment found for the user: '%s'", user.getUsername()));
            return;
        }
        StorageEquipment storageEquipment = equipmentForUse.get();

        UserPowerConsumption consumption = userPowerConsumptionRepository.findByUserAndEndIsNull(user);
        if ( user.getPowerConsumptionPerMinute() > 0 ){
            if( consumption == null ){
                consumption = new UserPowerConsumption();
                consumption.setUser(user);
                consumption.setBegin(new Date());
                logger.info(String.format("user: '%s' increased usage", user.getUsername()));
            }

        }else{
            if( consumption != null ){
                logger.info(String.format("user: '%s' terminated the use", user.getUsername()));
                consumption.setEnd(new Date());
            }
        }

        if(consumption!=null){
            consumption.setTotal(consumption.getTotal() + user.getPowerConsumptionPerMinute());
            storageEquipment.decrease(user.getPowerConsumptionPerMinute());
            userPowerConsumptionRepository.save(consumption);
            storageEquipmentRepository.save(storageEquipment);
        }
    }
}
