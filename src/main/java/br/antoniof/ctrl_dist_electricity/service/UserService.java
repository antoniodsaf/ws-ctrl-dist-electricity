package br.antoniof.ctrl_dist_electricity.service;

import br.antoniof.ctrl_dist_electricity.model.User;

import java.util.List;

public interface UserService {
    User findById(String id);
    User findByUsername(String username);
    List<User> findAll ();

    void increaseConsumption(User user);
}
