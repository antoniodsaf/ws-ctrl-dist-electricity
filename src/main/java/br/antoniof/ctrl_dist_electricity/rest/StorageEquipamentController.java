package br.antoniof.ctrl_dist_electricity.rest;

import br.antoniof.ctrl_dist_electricity.model.StorageEquipment;
import br.antoniof.ctrl_dist_electricity.model.User;
import br.antoniof.ctrl_dist_electricity.repository.StorageEquipmentRepository;
import br.antoniof.ctrl_dist_electricity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping( value = "/storages" )
public class StorageEquipamentController extends RESTController<StorageEquipment, String> {

    @Autowired
    public StorageEquipamentController(StorageEquipmentRepository repo) {
        super(repo);
    }
}