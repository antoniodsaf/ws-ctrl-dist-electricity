package br.antoniof.ctrl_dist_electricity.rest;

import br.antoniof.ctrl_dist_electricity.model.User;
import br.antoniof.ctrl_dist_electricity.model.UserPowerConsumption;
import br.antoniof.ctrl_dist_electricity.repository.UserPowerConsumptionRepository;
import br.antoniof.ctrl_dist_electricity.repository.UserRepository;
import br.antoniof.ctrl_dist_electricity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping( value = "/users" )
@PreAuthorize("hasRole('USER')")
public class UserController extends RESTController<User, String> {

    @Autowired
    public UserController(UserRepository repo) {
        super(repo);
    }

    @Autowired
    private UserPowerConsumptionRepository userPowerConsumptionRepository;

    @RequestMapping(value="/{id}/consumptions", method= RequestMethod.GET)
    public @ResponseBody
    List<UserPowerConsumption> consumptions(@PathVariable String id) {
        User user = new User();
        user.setId(id);
        return this.userPowerConsumptionRepository.findByUser(user);
    }

    @RequestMapping(method = GET, value="/whoami")
    @PreAuthorize("hasRole('USER')")
    public User user() {
        return (User)SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
    }
}