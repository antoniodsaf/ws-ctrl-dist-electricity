package br.antoniof.ctrl_dist_electricity.rest;

import br.antoniof.ctrl_dist_electricity.model.UserPowerConsumption;
import br.antoniof.ctrl_dist_electricity.repository.UserPowerConsumptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by antonio on 26/12/17.
 */
@RestController
@RequestMapping( value = "/user-power-consumptions" )
public class UserPowerConsumptionController {

    @Autowired
    private UserPowerConsumptionRepository repo;

    @RequestMapping(method = GET)
    public List<UserPowerConsumption> findAll() {
        return repo.findAll();
    }

}