package br.antoniof.ctrl_dist_electricity.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.AllowableListValues;
import springfox.documentation.service.ApiDescription;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingScannerPlugin;
import springfox.documentation.spi.service.contexts.DocumentationContext;
import springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.Sets.newHashSet;

@Configuration
public class LoginRequestSwagger implements ApiListingScannerPlugin {

    @Override
    public List<ApiDescription> apply(DocumentationContext context) {
        return new ArrayList<ApiDescription>(
                Arrays.asList(
                        new ApiDescription(
                                "/login",
                                "Return a valid JWT token, which expires in 10 minutes, that must be used to " +
                                        "authenticate when calling api methods.\n\n" +
                                        "It must be sent in header request e.g: \n\n    Authorization: Bearer TOKEN\n\n" +
                                        "replacing TOKEN by a valid token",
                                Arrays.asList( //<2>
                                        new OperationBuilder(
                                                new CachingOperationNameGenerator())
                                                .position(0)
                                                .summary("Login/Auth Token")
                                                .tags(newHashSet("# Login/Auth Token"))
                                                .authorizations(new ArrayList())
                                                .codegenMethodNameStem("tokenAuthPOST") //<3>
                                                .method(HttpMethod.POST)
                                                .notes("Create a new token")
                                                .consumes(newHashSet(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                                                .parameters(
                                                        Arrays.asList( //<4>
                                                                new ParameterBuilder()
                                                                        .description("Login of user")
                                                                        .name("username")
                                                                        .parameterType("form")
                                                                        .required(true)
                                                                        .modelRef(new ModelRef("string"))
                                                                        .type(new TypeResolver().resolve(String.class))
                                                                        .defaultValue("antonio")
                                                                        .build(),
                                                                new ParameterBuilder()
                                                                        .description("Password of user")
                                                                        .name("password")
                                                                        .parameterType("form")
                                                                        .required(true)
                                                                        .modelRef(new ModelRef("string"))
                                                                        .type(new TypeResolver().resolve(String.class))
                                                                        .defaultValue("xpto")
                                                                        .build())
                                                ).build()),
                                false)));
    }

    @Override
    public boolean supports(DocumentationType delimiter) {
        return DocumentationType.SWAGGER_2.equals(delimiter);
    }

}