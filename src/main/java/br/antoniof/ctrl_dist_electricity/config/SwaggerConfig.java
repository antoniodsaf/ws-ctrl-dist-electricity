package br.antoniof.ctrl_dist_electricity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

/**
 * Created by antonio on 26/12/17.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .consumes(newHashSet("application/json"))
                .produces(newHashSet("application/json"))
                .apiInfo(apiInfo())
                .globalOperationParameters(headerConfiguration());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Control Distribution Electricity",
                "It's a project of control and distribution of electric energy",
                "API ctrl_dist_electricity",
                "Terms of service",
                new springfox.documentation.service.Contact("Antonio Filho", "http://bitbucket.org/antoniodsaf", "antoniodsaf@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }

    public List<springfox.documentation.service.Parameter> headerConfiguration() {
        return newArrayList(new ParameterBuilder()
                .name("Authorization")
                .description("JWT security token obtained from the service method api/token." +
                        "Token must be sent in header request e.g: Authorization: Bearer <TOKEN>" +
                        "replacing <TOKEN> by a valid token")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(true).build());
    }

}